class Player {
  constructor(playerId, name) {
    this.playerId = playerId;
    this.displayName = name;

    this.heroes = [];
    this.heroGemType = new Set();
  }

  getTotalHeroAlive() {
    return this.getHerosAlive().length;
  }

  getHerosAlive() {
    return this.heroes.filter((hero) => hero.isAlive());
  }
  getCountHerosAlive() {
    const heros = this.heroes.filter((hero) => hero.isAlive());
    return heros.length;
  }

  getCastableHeros() {
    let arr = this.heroes.filter((hero) => hero.isAlive() && hero.isFullMana());
    return arr;
  }

  playerMostAttack() {
    const heros = this.getHerosAlive();
    return heros.reduce((preHero, curHero) => {
      return preHero.attack > curHero.attack ? preHero : curHero;
    }, heros[0]);
  }

  anyHeroFullMana() {
    let arr = this.heroes.filter((hero) => hero.isAlive() && hero.isFullMana());

    let hero =
      arr != null && arr != undefined && arr.length > 0 ? arr[0] : null;
    return hero;
  }

  isTrueHeroFullMana() {
    let arr = this.heroes.filter((hero) => hero.isAlive() && hero.isFullMana());
    if (arr.length >= 2) return true;
    return false;
  }

  firstHeroAlive() {
    let arr = this.heroes.filter((hero) => hero.isAlive());

    let hero =
      arr != null && arr != undefined && arr.length > 0 ? arr[0] : null;
    return hero;
  }
  secondHeroAlive() {
    let arr = this.heroes.filter((hero) => hero.isAlive());

    let hero =
      arr != null && arr != undefined && arr.length > 1 ? arr[1] : null;
    return hero;
  }

  getRecommendGemType() {
    this.heroGemType = new Set();

    for (let i = 0; i < this.heroes.length; i++) {
      let hero = this.heroes[i];

      for (let j = 0; j < hero.gemTypes.length; j++) {
        let gt = hero.gemTypes[j];
        this.heroGemType.add(GemType[gt]);
      }
    }

    return this.heroGemType;
  }

  getCurrentGemType() {
    const heroGemType = new Set();

    const herosAlive = this.getHerosAlive();
    const heros = herosAlive.filter((hero) => !hero.isFullMana());

    for (let i = 0; i < heros.length; i++) {
      let hero = heros[i];

      for (let j = 0; j < hero.gemTypes.length; j++) {
        let gt = hero.gemTypes[j];
        heroGemType.add(GemType[gt]);
      }
    }

    return Array.from(heroGemType);
  }

  getHerosFullMana() {
    const heros = this.heroes.filter((hero) => hero.isAlive() && hero.isFullMana());
    return heros;
  }

  firstAliveHeroCouldReceiveMana(type) {
    const res = this.heroes.find(
      (hero) => hero.isAlive() && hero.couldTakeMana(type)
    );
    return res;
  }

  clone() {
    const cloned = new Player(this.playerId, this.displayName);
    cloned.heroes = this.heroes.map((hero) => hero.clone());
    cloned.heroGemType = new Set(Array.from(this.heroGemType));
    return cloned;
  }
}
