function canGetExtraturn(grid) {
  const listMatch = grid.suggestMatch();
  if (listMatch.length === 0) return false;
  else {
    const matchSizeThanFour = listMatch.find((match) => match.sizeMatch > 4);
    if (matchSizeThanFour) return true;

    const matchGemContainXT = listMatch.find((gemMatch) =>
      gemMatch.modifiers.includes(GemModifier.EXTRA_TURN)
    );
    if (matchGemContainXT) return true;
    return false;
  }
}

function checkCanSwapToKillEnemy(grid, botPlayer, enemyPlayer) {
  const listMatch = grid.suggestMatch();
  let matchSword = listMatch.find(
    (match) => match.type === GemType.SWORD && match.sizeMatch > 3
  );
  matchSword = matchSword
    ? matchSword
    : listMatch.find((match) => match.type === GemType.SWORD);
  if (
    (matchSword &&
      matchSword.sizeMatch > 3 &&
      botPlayer.firstHeroAlive().attack + 5 >=
        enemyPlayer.firstHeroAlive().hp) ||
    (matchSword &&
      botPlayer.firstHeroAlive().attack >= enemyPlayer.firstHeroAlive().hp)
  )
    return { possiple: true, swap: matchSword };
  return { possiple: false };
}

function findIndexForNefia(grid, botPlayer) {
  let index = 49;
  const gems = grid.gems;
  const benefits = [];
  for (let x = 0; x < 64; x++) {
    if (
      gems[x].index === gems[x].x ||
      gems[x].index === gems[x].x + 56 ||
      gems[x].index === gems[x].y * 8 ||
      gems[x].index === (gems[x].y + 1) * 7 + gems[x].y
    ) {
      continue;
    }
    benefits.push(areaBenefit(x));
  }

  for (let x = 0; x < benefits.length; x++) {
    //--------------------------------
    // find area that make extra turn
    //--------------------------------
    if (benefits[x].extraTurn) return benefits[x].index;
    if (
      benefits[x].color.sword > 4 ||
      benefits[x].color.green > 4 ||
      benefits[x].color.yellow > 4 ||
      benefits[x].color.red > 4 ||
      benefits[x].color.purple > 4 ||
      benefits[x].color.blue > 4 ||
      benefits[x].color.brown > 4
    )
      return benefits[x].index;

    //----------------------------------
    // find area that contain many sword
    //----------------------------------
    if (benefits[x].color.sword >= 3) return benefits[x].index;

    //------------------------------------
    // find area that contain many own gem
    //------------------------------------
    const currentGems = botPlayer.getCurrentGemType();
    for (let i = 0; i < currentGems.length; i++) {
      if (benefits[x].typeMax === currentGems[i]) return benefits[x].index;
    }
    for (let i = 0; i < currentGems.length; i++) {
      if (benefits[x].modifier) return benefits[x].index;
    }
  }

  return index;
}

function findTargetForFireSpirate(grid, enemyPlayer) {
  let targetHeroId = enemyPlayer.playerMostAttack().id.toString();
  let gems = grid.gems;

  //----------------
  // Simulate result
  //----------------

  // tìm gem đỏ
  let redGem = 0;
  gems.map((gem) => {
    if (gem.type === GemType.RED) redGem++;
  });

  // tìm attack đạt đc, index, hp đối thủ
  const eHeros = enemyPlayer.getHerosAlive();
  const herosBeHit = enemyPlayer.getHerosAlive().map((hero) => ({
    id: hero.id,
    attackReceive: hero.attack + redGem,
    hp: hero.hp,
    hero,
  }));

  let eHerosCanKill = [];
  for (let x = 0; x < herosBeHit.length; x++) {
    if (herosBeHit[x].attackReceive >= herosBeHit[x].hp) {
      eHerosCanKill.push(herosBeHit[x]);
    }
  }

  for (hr of eHerosCanKill) {
    if (hr.hero.isFullMana() && hr.id === "CERBERUS") return hr.id.toString();
  }
  for (hr of eHerosCanKill) {
    if (hr.hero.isFullMana() && hr.id === "THUNDER_GOD")
      return hr.id.toString();
  }
  for (hr of eHerosCanKill) {
    if (hr.hero.isFullMana() && hr.id === "FIRE_SPIRIT")
      return hr.id.toString();
  }
  for (hr of eHerosCanKill) {
    if (hr.hero.isFullMana() && hr.id === "MERMAID") return hr.id.toString();
  }
  for (hr of eHerosCanKill) {
    if (hr.hero.isFullMana() && hr.id !== "SEA_SPIRIT" && hr.id !== "MONK")
      return hr.id.toString();
  }
  for (hr of eHerosCanKill) {
    if (hr.id === "CERBERUS") return hr.id.toString();
  }
  for (hr of eHerosCanKill) {
    if (hr.id === "THUNDER_GOD") return hr.id.toString();
  }
  for (hr of eHerosCanKill) {
    if (hr.id === "FIRE_SPIRIT") return hr.id.toString();
  }
  for (hr of eHerosCanKill) {
    if (hr.id === "MERMAID") return hr.id.toString();
  }

  const target = eHerosCanKill.reduce((pr, cur) => {
    if (pr.attackReceive - pr.hp < cur.attackReceive - cur.hp) return pr;
    else return cur;
  }, eHerosCanKill[0]);
  if (target && target.id !== targetHeroId) return target.id.toString();
  

   for( let hr of eHeros){
      if (hr.id === "CERBERUS") return hr.id.toString();
   }
   for( let hr of eHeros){
      if (hr.id === "THUNDER_GOD") return hr.id.toString();
   }
   for( let hr of eHeros){
      if (hr.id === "FIRE_SPIRIT") return hr.id.toString();
   }
   for( let hr of eHeros){
      if (hr.id === "MERMAID") return hr.id.toString();
   }

    return targetHeroId;
}

function findTargetForSkeleton(botPlayer, enemyPlayer) {
  // init cast data
  let dataCast = {
    targetId: undefined,
    isTargetAllyOrNot: false,
  };
  const enemyHeros = enemyPlayer.getHerosAlive();

  // find the enemy hero with the most hp
  const eHero = enemyHeros.reduce((pre, cur) => {
    return pre.hp > cur.hp ? pre : cur;
  }, enemyHeros[0]);

  // in case of last hero, exchange hp with the enemy hero with the most hp
  const botHeros = botPlayer.getHerosAlive();
  if (botHeros.length === 1 && botHeros[0].hp < eHero.hp)
    dataCast = {
      targetId: eHero.id.toString(),
      isTargetAllyOrNot: false,
    };

  if (botHeros.length > 1) {
    const skeleton = botHeros.find((hero) => hero.id === "SKELETON");

    if (skeleton.hp < eHero.hp) {
      dataCast = {
        targetId: eHero.id.toString(),
        isTargetAllyOrNot: false,
      };
    } else {
      botHeros.map((hero) => {
        if (hero.hp < skeleton.hp)
          dataCast = {
            targetId: hero.id.toString(),
            isTargetAllyOrNot: true,
          };
      });
    }

    // chua chac chan
    // check máu đông mình nhiều hay đối thủ nhiều
    if (dataCast.targetId === undefined && skeleton.hp < eHero.hp)
      dataCast = {
        targetId: eHero.id.toString(),
        isTargetAllyOrNot: false,
      };
  }
  return dataCast;
}

function findTargetForFate(enemyPlayer) {
  const enemyHeros = enemyPlayer.getHerosAlive();
  let targetId = enemyHeros[0].id.toString();
  const targetIds = [
    "SEA_GOD",
    "CERBERUS",
    "THUNDER_GOD",
    "FIRE_SPIRIT",
    "DISPATER",
  ];

  for (let i = 0; i < targetIds.length; i++) {
    const tmp = targetId;
    for (let x = 0; x < enemyHeros.length; x++) {
      if (enemyHeros[x].id === targetIds[i]) {
        targetId = enemyHeros[x].id.toString();
        break;
      }
    }
    if (tmp !== targetId) break;
  }

  return targetId;
}

function areaBenefit(grid, index) {
  const benefit = {
    index,
    modifier: false,
    extraTurn: false,
    color: {
      sword: 0,
      green: 0,
      yellow: 0,
      red: 0,
      purple: 0,
      blue: 0,
      brown: 0,
    },
    typeMax: -1,
  };

  for (let x = index + 7; x <= index + 9; x++) {
    if (grid.gems[x].modifier === GemModifier.EXTRA_TURN)
      benefit.extraTurn = true;
    if (grid.gems[x].modifier) benefit.modifier = true;
    switch (grid.gems[x].type) {
      case GemType.SWORD:
        benefit.color.sword++;
        break;
      case GemType.GREEN:
        benefit.color.green++;
        break;
      case GemType.YELLOW:
        benefit.color.yellow++;
        break;
      case GemType.RED:
        benefit.color.red++;
        break;
      case GemType.PURPLE:
        benefit.color.purple++;
        break;
      case GemType.BLUE:
        benefit.color.blue++;
        break;
      case GemType.BROWN:
        benefit.color.brown++;
        break;
    }
  }
  for (let x = index - 1; x <= index + 1; x++) {
    if (grid.gems[x].modifier === GemModifier.EXTRA_TURN)
      benefit.extraTurn = true;
    if (grid.gems[x].modifier) benefit.modifier = true;
    switch (grid.gems[x].type) {
      case GemType.SWORD:
        benefit.color.sword++;
        break;
      case GemType.GREEN:
        benefit.color.green++;
        break;
      case GemType.YELLOW:
        benefit.color.yellow++;
        break;
      case GemType.RED:
        benefit.color.red++;
        break;
      case GemType.PURPLE:
        benefit.color.purple++;
        break;
      case GemType.BLUE:
        benefit.color.blue++;
        break;
      case GemType.BROWN:
        benefit.color.brown++;
        break;
    }
  }
  for (let x = index - 9; x <= index + -7; x++) {
    if (grid.gems[x].modifier === GemModifier.EXTRA_TURN)
      benefit.extraTurn = true;
    if (grid.gems[x].modifier) benefit.modifier = true;
    switch (grid.gems[x].type) {
      case GemType.SWORD:
        benefit.color.sword++;
        break;
      case GemType.GREEN:
        benefit.color.green++;
        break;
      case GemType.YELLOW:
        benefit.color.yellow++;
        break;
      case GemType.RED:
        benefit.color.red++;
        break;
      case GemType.PURPLE:
        benefit.color.purple++;
        break;
      case GemType.BLUE:
        benefit.color.blue++;
        break;
      case GemType.BROWN:
        benefit.color.brown++;
        break;
    }
  }
  let typeMax = -1;
  for (const color in benefit.color) {
    if (benefit.color[color] > typeMax) typeMax = benefit.color[color];
  }
  benefit.typeMax = typeMax;
  return benefit;
}

function getSwapGem(grid, botPlayer) {
  let listMatchGem = grid.suggestMatch();
  // const gridClone = grid.clone();

  if (listMatchGem.length === 0) {
    return [-1, -1];
  }

  // check 5 gem
  let matchGemSizeThanFour = listMatchGem.find(
    (gemMatch) => gemMatch.sizeMatch > 4
  );
  if (matchGemSizeThanFour) {
    return matchGemSizeThanFour;
  }

  // Check extra turn
  let matchGemContainXT = listMatchGem.find((gemMatch) =>
    gemMatch.modifiers.includes(GemModifier.EXTRA_TURN)
  );
  if (matchGemContainXT) {
    return matchGemContainXT;
  }

  const listSimulate = listMatchGem.map((matchGem) => {
    let performSwap = grid
      .clone()
      .performSwap(matchGem.index1, matchGem.index2);
    return {
      matchGem,
      performSwap,
    };
  });

  for (simulate of listSimulate) {
    for (ins of simulate.performSwap) {
      if (ins.isExtraTurn) {
        console.log("have turn bonus!!!");
        return simulate.matchGem;
      }
    }
  }

  // Check 4 gem and include own gem type
  const currentGems = botPlayer.getCurrentGemType();

  for (let i = 0; i < currentGems.length; i++) {
    const sizeThanThree1 = listMatchGem.find(
      (gemMatch) =>
        gemMatch.sizeMatch > 3 &&
        gemMatch.type === currentGems[i] &&
        gemMatch.modifiers.length > 0
    );
    if (sizeThanThree1) {
      return sizeThanThree1;
    }
  }

  for (let i = 0; i < currentGems.length; i++) {
    const sizeThanThree3 = listMatchGem.find(
      (gemMatch) => gemMatch.sizeMatch > 3 && gemMatch.type === GemType.SWORD
    );
    if (sizeThanThree3) {
      return sizeThanThree3;
    }
  }
  //////////////////
  // check domino has own gem
  let tmp = listSimulate[0];
  for (simulate of listSimulate) {
    if (tmp.performSwap.length < simulate.performSwap.length) {
      tmp = simulate;
    }
  }
  console.log(listSimulate, "best length 1");
  console.log(tmp, "best length");
  if (tmp.performSwap.length > 1) {
    if (tmp.performSwap.length >= 3) return tmp.matchGem;
    let count = 0;
    let check = false;
    for (let pf of tmp.performSwap) {
      for (let i = 0; i < currentGems.length; i++) {
        if (currentGems[i] === pf.type || pf.type === GemType.SWORD) {
          count++;
          check = count > 1 ? true : false;
        }
      }
      // if(currentGems.includes(pf.type))
      //   return tmp.matchGem;
    }
    if (check) return tmp.matchGem;
  }

  //////////////////
  for (let i = 0; i < currentGems.length; i++) {
    const sizeThanThree2 = listMatchGem.find(
      (gemMatch) => gemMatch.sizeMatch > 3 && gemMatch.type === currentGems[i]
    );
    if (sizeThanThree2) {
      return sizeThanThree2;
    }
  }

  // Check sword gem
  if (botPlayer.getHerosAlive().length <= 2) {
    let matchGemSword = listMatchGem.find(
      (gemMatch) => gemMatch.type == GemType.SWORD
    );
    if (matchGemSword) {
      return matchGemSword;
    }
  }

  // Check own gem type
  for (let i = 0; i < currentGems.length; i++) {
    const matchOwnGem1 = listMatchGem.find(
      (gemMatch) =>
        gemMatch.type === currentGems[i] && gemMatch.modifiers.length > 0
    );
    if (matchOwnGem1) {
      return matchOwnGem1;
    }
  }
  for (let i = 0; i < currentGems.length; i++) {
    const matchOwnGem2 = listMatchGem.find(
      (gemMatch) => gemMatch.type === currentGems[i]
    );
    if (matchOwnGem2) {
      return matchOwnGem2;
    }
  }

  // Check modifier
  let matchGemModifier = listMatchGem.find(
    (gemMatch) => gemMatch.modifiers.length > 0
  );

  if (matchGemModifier) {
    return matchGemModifier;
  }

  // default
  return listMatchGem[0];
}
